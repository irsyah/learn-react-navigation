import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { AntDesign, Entypo } from '@expo/vector-icons';

import ProfileScreen from './screens/ProfileScreen';
import HomeStackNavigator from './components/HomeStackNavigator';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen 
          name='Home' 
          component={HomeStackNavigator}
          options = {{ 
            headerShown: false, 
            tabBarIcon: ({ color, size}) => (
            <AntDesign name="home" size={24} color="black" />
          ) }}
        />
        <Tab.Screen 
          name='Profile' 
          component={ProfileScreen} 
          options = {{ tabBarIcon: ({ color, size}) => (
            <Entypo name="user" size={24} color="black" />
          )}}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
