import { View, Text, Button } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function HomeScreen() {
  const navigation = useNavigation();
  const goToDetails = () => {
    navigation.navigate('Details')
  }

  return (
    <SafeAreaView>
      <Text>HomeScreen</Text>
      <Button title="Go To Details" onPress={goToDetails}/>
    </SafeAreaView>
  )
}