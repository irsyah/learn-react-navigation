## Requirement
Buatlah BottomTab(Tab Navigation) dengan Menu:
    - Movie
    - Profile
    - Favorites

Buatlah Stack Navigation
Pada MovieScreen buatlah stack navigation untuk
    Movie Details

Pada FavoriteScreen buatlah stack navigation 
    Favorite Details

## Documentation
https://reactnavigation.org/docs/stack-navigator
https://reactnavigation.org/docs/bottom-tab-navigator
